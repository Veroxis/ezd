package main

import (
	_ "embed"

	"gitlab.com/veroxis/ezd/src/cmd"
)

func main() {
	cmd.Execute()
}

package dockerseq

// #cgo LDFLAGS: -L${SRCDIR}/../../../third_party/lib -ldockerseq
// #cgo CFLAGS: -flto
// #include "../../../third_party/lib/dockerseq.h"
import "C"

import (
	"encoding/json"
	"fmt"
	"unsafe"

	"gitlab.com/veroxis/ezd/src/config"
)

func RunDockerSeq(conf *config.DockerSeqConfig) error {
	jsonConf, err := json.Marshal(*conf)
	if err != nil {
		return err
	}
	confCString := C.CString(string(jsonConf))
	ffiResponse := C.DS_run(confCString)
	defer C.free(unsafe.Pointer(confCString))
	runResult := C.GoString(ffiResponse)
	if runResult != "" {
		return fmt.Errorf(runResult)
	}
	return nil
}

package cmdrun

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"gitlab.com/veroxis/ezd/src/config"
	"gitlab.com/veroxis/ezd/src/shell"
)

var errShellExecRun = errors.New("ShellExecRun")

func ErrShellExecRun(msg string) error {
	return fmt.Errorf("%w: %s", errShellExecRun, msg)
}

func execShellRun(config *config.AppConfiguration, commandName string, forwardedArgs []string) error {
	fmt.Fprintf(os.Stdout, "%s[Executing shell_exec task: `%s%s%s%s`]%s\n",
		shell.CodeColorBlue, shell.CodeBold, commandName, shell.CodeReset, shell.CodeColorBlue, shell.CodeReset)
	os.Setenv("EZD_FORWARDED_ARGS", strings.Join(forwardedArgs, " "))
	for _, envString := range config.Tasks.ShellExec[commandName].Env {
		amountPieces := 2
		envParts := strings.SplitN(envString, "=", amountPieces)
		if len(envParts) == amountPieces {
			os.Setenv(envParts[0], envParts[1])
		}
	}
	for _, cmd := range config.Tasks.ShellExec[commandName].Commands {
		fmt.Fprintf(os.Stdout, "[%sezd%s:%sshell_exec%s] %s>_ %s%s%s\n",
			shell.CodeColorGreen, shell.CodeReset, shell.CodeColorGreen, shell.CodeReset, shell.CodeColorBlue, shell.CodeColorPurple, cmd, shell.CodeReset)
		status, err := shell.AttachedExec("/usr/bin/env", "sh", "-c", cmd)
		if err != nil {
			return ErrShellExecRun("internal.AttachedExec(\"/usr/bin/env\", \"sh\", \"-c\", cmd) failed: " + err.Error())
		}
		if !status.ProcessState.Success() {
			return ErrShellExecRun("internal.AttachedExec(\"/usr/bin/env\", \"sh\", \"-c\", cmd) failed: exit code was not 0")
		}
	}
	fmt.Fprintf(os.Stdout, "%s[Finished shell_exec task: `%s%s%s%s`]%s\n",
		shell.CodeColorBlue, shell.CodeBold, commandName, shell.CodeReset, shell.CodeColorBlue, shell.CodeReset)

	return nil
}

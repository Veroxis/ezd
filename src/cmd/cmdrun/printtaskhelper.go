package cmdrun

import (
	"fmt"
	"os"

	"gitlab.com/veroxis/ezd/src/config"
	"gitlab.com/veroxis/ezd/src/helpers"
	"gitlab.com/veroxis/ezd/src/shell"
)

func printAvailableTasksHelperMenu(appConfig *config.AppConfiguration, userSelectedCommand string) {
	fmt.Fprintf(os.Stdout, "[%sezd%s] %sTask `%s%s%s` does not exist.%s\n",
		shell.CodeColorGreen, shell.CodeReset, shell.CodeColorGreen, shell.CodeColorPurple, userSelectedCommand, shell.CodeColorGreen, shell.CodeReset)
	allTasks := appConfig.GetAllCommandNames()
	similarTasks := []string{}
	for _, task := range allTasks {
		if helpers.IsSimilar(task, userSelectedCommand) {
			similarTasks = append(similarTasks, task)
		}
	}
	if len(similarTasks) > 0 {
		fmt.Fprintf(os.Stdout, "[%sezd%s] %sDid you mean any of these?%s\n", shell.CodeColorGreen, shell.CodeReset, shell.CodeColorGreen, shell.CodeReset)
		for _, task := range similarTasks {
			fmt.Fprintf(os.Stdout, "[%sezd%s]    `%s%s%s`\n", shell.CodeColorGreen, shell.CodeReset, shell.CodeColorPurple, task, shell.CodeReset)
		}
	} else {
		fmt.Fprintf(os.Stdout, "[%sezd%s] %sNo similar task was found. These are available:%s\n", shell.CodeColorGreen, shell.CodeReset, shell.CodeColorGreen, shell.CodeReset)
		fmt.Fprintf(os.Stdout, "[%sezd%s] ", shell.CodeColorGreen, shell.CodeReset)
		for _, task := range allTasks {
			fmt.Fprintf(os.Stdout, "`%s%s%s` ", shell.CodeColorPurple, task, shell.CodeReset)
		}
		fmt.Fprintf(os.Stdout, "\n")
	}
}

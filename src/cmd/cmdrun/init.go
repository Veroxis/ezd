package cmdrun

import (
	"github.com/spf13/cobra"
	"gitlab.com/veroxis/ezd/src/runtime"
	"gitlab.com/veroxis/ezd/src/shell"
)

func InitRunConfigs(cmd *cobra.Command) {
	verbose, err := cmd.Flags().GetBool("verbose")
	if err != nil {
		runtime.ErrorExit(err)
	}
	shell.VerboseMode = verbose
}

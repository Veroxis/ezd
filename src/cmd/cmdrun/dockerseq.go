package cmdrun

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"gitlab.com/veroxis/ezd/src/config"
	"gitlab.com/veroxis/ezd/src/ffi/dockerseq"
	"gitlab.com/veroxis/ezd/src/shell"
)

var errDockerSeq = errors.New("DockerSeq")

func ErrDockerSeq(msg string) error {
	return fmt.Errorf("%w: %s", errDockerSeq, msg)
}

func execDockerSeq(config *config.AppConfiguration, commandName string, forwardedArgs []string) error {
	commandConfig, keyExists := config.Tasks.DockerSeq[commandName]
	if !keyExists {
		return ErrDockerSeq("command does not exist: " + commandName)
	}
	commandConfig.Config.Env = append(commandConfig.Config.Env, "EZD_FORWARDED_ARGS="+strings.Join(forwardedArgs, " "))
	fmt.Fprintf(os.Stdout, "%s[Executing docker_seq task: `%s%s%s%s`]%s\n",
		shell.CodeColorBlue, shell.CodeBold, commandName, shell.CodeReset, shell.CodeColorBlue, shell.CodeReset)
	defer fmt.Fprintf(os.Stdout, "%s[Finished docker_seq task: `%s%s%s%s`]%s\n",
		shell.CodeColorBlue, shell.CodeBold, commandName, shell.CodeReset, shell.CodeColorBlue, shell.CodeReset)
	err := dockerseq.RunDockerSeq(&commandConfig.Config)
	if err != nil {
		return ErrDockerSeq("command failed: " + commandName)
	}
	return nil
}

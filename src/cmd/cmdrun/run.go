package cmdrun

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/veroxis/ezd/src/config"
)

var errTaskRun = errors.New("TaskRun")

func ErrTaskRun(msg string) error {
	return fmt.Errorf("%w: %s", errTaskRun, msg)
}

func Run(cmd *cobra.Command, args []string, appConfig *config.AppConfiguration) error {
	userSelectedCommand, forwardedArgs, err := parseArgs(args, cmd.ArgsLenAtDash())
	if err != nil {
		return ErrTaskRun(err.Error())
	}

	if !appConfig.TaskExists(userSelectedCommand) {
		printAvailableTasksHelperMenu(appConfig, userSelectedCommand)
		return ErrTaskRun("failed to run task `" + userSelectedCommand + "`")
	}

	if _, keyExists := appConfig.Tasks.ShellExec[userSelectedCommand]; keyExists {
		err := execShellRun(appConfig, userSelectedCommand, forwardedArgs)
		if err != nil {
			return ErrTaskRun(err.Error())
		}
	}

	if _, keyExists := appConfig.Tasks.DockerSeq[userSelectedCommand]; keyExists {
		err := execDockerSeq(appConfig, userSelectedCommand, forwardedArgs)
		if err != nil {
			return ErrTaskRun(err.Error())
		}
	}

	return nil
}

package cmdrun

import (
	"errors"
	"fmt"
	"strings"
)

var errParseArgs = errors.New("ParseArgs")

func ErrParseArgs(msg string) error {
	return fmt.Errorf("%w: %s", errParseArgs, msg)
}

func parseArgs(args []string, splitAt int) (command string, forwardedArgs []string, err error) {
	if splitAt < 0 {
		// there are no forwarded arguments

		if len(args) != 1 {
			// without forwarded arguments, there has to be exactly 1 element
			amountTasks := fmt.Sprint(len(args))
			taskList := "[" + strings.Join(args, ", ") + "]"

			return "", nil, ErrParseArgs("expected 1 task, got " + amountTasks + ": " + taskList)
		}

		// exactly one argument was given, this is save
		return args[0], nil, nil
	}

	// there are forwarded arguments
	tasks, forwardedArguments := args[:splitAt], args[splitAt:]

	if len(tasks) != 1 {
		// without forwarded arguments, there should be exactly 1 element
		amountTasks := fmt.Sprint(len(tasks))
		taskList := "[" + strings.Join(tasks, ", ") + "]"

		return "", nil, ErrParseArgs("expected 1 task, got " + amountTasks + ": " + taskList)
	}

	return tasks[0], forwardedArguments, nil
}

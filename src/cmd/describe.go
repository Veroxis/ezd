package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/veroxis/ezd/src/cmd/cmddescribe"
	"gitlab.com/veroxis/ezd/src/runtime"
)

var (
	describeCmd = &cobra.Command{
		Use:   "describe",
		Short: "Print details about the current project configuration",
		Run: func(cmd *cobra.Command, args []string) {
			appConfig, err := GlobalCommandInit()
			if err != nil {
				runtime.ErrorExit(err)
			}
			cmddescribe.InitDescribeConfigs(cmd)
			err = cmddescribe.Run(cmd, args, appConfig)
			if err != nil {
				runtime.ErrorExit(err)
			}
		},
	}
)

func init() {
	describeCmd.Flags().BoolP("details", "d", false, "displays the full parsed configuration")
	describeCmd.Flags().BoolP("plain", "p", false, "disable syntax highlighting for printed yaml")

	rootCmd.AddCommand(describeCmd)
}

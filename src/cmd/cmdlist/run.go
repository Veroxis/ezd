package cmdlist

import (
	"fmt"
	"os"

	"gitlab.com/veroxis/ezd/src/config"
)

func Run(appConfig *config.AppConfiguration) {
	for _, task := range appConfig.GetAllCommandNames() {
		fmt.Fprintf(os.Stdout, "%s ", task)
	}
	fmt.Fprintf(os.Stdout, "\n")
}

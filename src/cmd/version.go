package cmd

import (
	_ "embed"
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/veroxis/ezd/src/runtime"
)

var (
	//go:embed "version.txt"
	version string
	//go:embed "buildtime.txt"
	buildtime string
	//go:embed "commithash.txt"
	commithash string
)

func init() {
	versionCmd.Flags().BoolP("verbose", "v", false, "enable verbose mode")
	rootCmd.AddCommand(versionCmd)
}

type versionData struct {
	Version   string `json:"version"`
	Hash      string `json:"hash"`
	BuildTime string `json:"build_time"`
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of `ezd`",
	Run: func(cmd *cobra.Command, args []string) {
		timestamp := strings.TrimSpace(buildtime)
		hash := strings.TrimSpace(commithash)
		version := strings.TrimSpace(version)
		verbose, err := cmd.Flags().GetBool("verbose")
		if err != nil {
			runtime.ErrorExit(err)
		}
		if verbose {
			versionData := &versionData{Version: version, Hash: hash, BuildTime: timestamp}
			versionDataJson, err := json.MarshalIndent(versionData, "", "  ")
			if err != nil {
				fmt.Println(err)
				return
			}
			fmt.Println(string(versionDataJson))
		} else {
			fmt.Fprintf(os.Stdout, "%s\n", version)
		}
	},
}

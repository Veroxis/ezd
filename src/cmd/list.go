package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/veroxis/ezd/src/cmd/cmdlist"
	"gitlab.com/veroxis/ezd/src/runtime"
)

var (
	listCmd = &cobra.Command{
		Use:   "list",
		Short: "Print all tasks defined in the project file",
		Run: func(cmd *cobra.Command, args []string) {
			appConfig, err := GlobalCommandInit()
			if err != nil {
				runtime.ErrorExit(err)
			}
			cmdlist.Run(appConfig)
		},
	}
)

func init() {
	rootCmd.AddCommand(listCmd)
}

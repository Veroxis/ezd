package cmddescribe

import (
	"errors"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/veroxis/ezd/src/config"
	"gitlab.com/veroxis/ezd/src/helpers"
	"gitlab.com/veroxis/ezd/src/runtime"
	"gitlab.com/veroxis/ezd/src/shell"
)

var errCommandDescribe = errors.New("CommandDescribe")

func ErrCommandDescribe(msg string) error {
	return fmt.Errorf("%w: %s", errCommandDescribe, msg)
}

func Run(cmd *cobra.Command, args []string, appConfig *config.AppConfiguration) error {
	if len(args) > 0 {
		for _, task := range args {
			if !helpers.StringSliceContains(appConfig.GetAllCommandNames(), task) {
				return ErrCommandDescribe("task does not exist: " + task)
			}
		}
		for _, task := range args {
			for shellTaskName, shellTask := range appConfig.Tasks.ShellExec {
				if shellTaskName == task {
					fmt.Fprintf(os.Stdout, "%sTask `%s`:%s\n\n", shell.CodeColorBlue, task, shell.CodeReset)
					printTaskDetails(shellTask)
				}
			}
			for dockerTaskName, dockerTask := range appConfig.Tasks.DockerSeq {
				if dockerTaskName == task {
					fmt.Fprintf(os.Stdout, "%sTask `%s`:%s\n---\n", shell.CodeColorBlue, task, shell.CodeReset)
					printTaskDetails(dockerTask)
				}
			}
		}

		return nil
	}
	if printDetails {
		printTaskDetails(appConfig)

		return nil
	}

	projectRoot, err := os.Getwd()
	if err != nil {
		runtime.PrintRuntimeError(false, "Can't get Project root: "+err.Error())
	}

	shortDescription := ShortDescription{}
	shortDescription.from(projectRoot, appConfig)

	printTaskDetails(shortDescription)

	return nil
}

func printTaskDetails(elem interface{}) {
	yml, err := helpers.AsYaml(elem)
	if err != nil {
		runtime.ErrorExit(err)
	}
	if !printPlain {
		yml, err = helpers.HighlightYaml(yml)
		if err != nil {
			runtime.ErrorExit(err)
		}
	}
	fmt.Fprintf(os.Stdout, "%s%s\n", yml, shell.CodeReset)
}

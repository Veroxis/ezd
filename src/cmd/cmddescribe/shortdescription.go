package cmddescribe

import (
	"sort"

	"gitlab.com/veroxis/ezd/src/config"
)

type ShortDescription struct {
	ProjectRoot string `yaml:"project_root"`
	Tasks       struct {
		ShellExec map[string]string `yaml:"shell_exec"`
		DockerSeq map[string]string `yaml:"docker_seq"`
	} `yaml:"tasks"`
}

func (c *ShortDescription) from(rootPath string, appConfig *config.AppConfiguration) {
	c.ProjectRoot = rootPath

	keys := []string{}
	for k := range appConfig.Tasks.ShellExec {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	if c.Tasks.ShellExec == nil {
		c.Tasks.ShellExec = map[string]string{}
	}
	for _, k := range keys {
		desc := "field `description` is empty"
		if appConfig.Tasks.ShellExec[k].Description != "" {
			desc = appConfig.Tasks.ShellExec[k].Description
		}
		c.Tasks.ShellExec[k] = desc
	}

	keys = []string{}
	for k := range appConfig.Tasks.DockerSeq {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	if c.Tasks.DockerSeq == nil {
		c.Tasks.DockerSeq = map[string]string{}
	}
	for _, k := range keys {
		desc := "field `description` is empty"
		if appConfig.Tasks.DockerSeq[k].Description != "" {
			desc = appConfig.Tasks.DockerSeq[k].Description
		}
		c.Tasks.DockerSeq[k] = desc
	}
}

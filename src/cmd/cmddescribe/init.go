package cmddescribe

import (
	"github.com/spf13/cobra"
	"gitlab.com/veroxis/ezd/src/runtime"
)

func InitDescribeConfigs(cmd *cobra.Command) {
	details, err := cmd.Flags().GetBool("details")
	if err != nil {
		runtime.ErrorExit(err)
	}
	printDetails = details

	plain, err := cmd.Flags().GetBool("plain")
	if err != nil {
		runtime.ErrorExit(err)
	}
	printPlain = plain
}

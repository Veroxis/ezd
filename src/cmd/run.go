package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/veroxis/ezd/src/cmd/cmdrun"
	"gitlab.com/veroxis/ezd/src/runtime"
)

var (
	runCmd = &cobra.Command{
		Use:   "run",
		Short: "Run one of the projects tasks",
		Run: func(cmd *cobra.Command, args []string) {
			appConfig, err := GlobalCommandInit()
			if err != nil {
				runtime.ErrorExit(err)
			}
			cmdrun.InitRunConfigs(cmd)
			err = cmdrun.Run(cmd, args, appConfig)
			if err != nil {
				runtime.ErrorExit(err)
			}
		},
	}
)

func init() {
	runCmd.Flags().BoolP("verbose", "v", false, "enable verbose mode")
	rootCmd.AddCommand(runCmd)
}

package cmd

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/veroxis/ezd/src/config"
	"gitlab.com/veroxis/ezd/src/runtime"
)

var (
	rootCmd = &cobra.Command{Use: "ezd"}
)

func init() {
	rootCmd.PersistentFlags().Bool("backtrace", false, "enable error traces")
}

func Execute() {
	// Init
	runtime.RegisterSignalHandler()
	defer runtime.Cleanup()

	// Showtime!
	if err := rootCmd.Execute(); err != nil {
		runtime.ErrorExit(err)
	}
}

var errGlobalCommandInit = errors.New("GlobalCommandInit")

func ErrGlobalCommandInit(msg string) error {
	return fmt.Errorf("%w: %s", errGlobalCommandInit, msg)
}

func GlobalCommandInit() (*config.AppConfiguration, error) {
	tracing, err := rootCmd.PersistentFlags().GetBool("backtrace")
	if err != nil {
		return nil, ErrGlobalCommandInit(err.Error())
	}
	runtime.EnableTracing = tracing

	appConfig, err := config.BuildAppConfiguration()
	if err != nil {
		return nil, ErrGlobalCommandInit(err.Error())
	}

	return appConfig, nil
}

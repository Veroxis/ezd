package config

type ShellExec struct {
	Env         []string `yaml:"env"`         // default: nil
	Description string   `yaml:"description"` // default: ""
	Commands    []string `yaml:"commands"`    // default: nil
}

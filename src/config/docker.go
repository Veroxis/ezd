package config

type PullStrategy string

const (
	Missing PullStrategy = "missing"
	Always  PullStrategy = "always"
	Never   PullStrategy = "never"
)

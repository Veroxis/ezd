package config

import (
	"errors"
	"fmt"
	"strings"

	"github.com/goccy/go-yaml"
	"gitlab.com/veroxis/ezd/src/debug"
	"gitlab.com/veroxis/ezd/src/ezdconstants"
	"gitlab.com/veroxis/ezd/src/helpers"
)

type AppConfiguration struct {
	Env   []string `yaml:"env"` // default: nil
	Tasks struct {
		ShellExec map[string]ShellExec `yaml:"shell_exec"` // default: nil
		DockerSeq map[string]DockerSeq `yaml:"docker_seq"` // default: nil
	} `yaml:"tasks"`
}

// Build the `*AppConfiguration`.
func BuildAppConfiguration() (*AppConfiguration, error) {
	configYml, err := findConfigGetContent()
	if err != nil {
		return nil, err
	}

	err = loadConstants(&configYml)
	if err != nil {
		return nil, err
	}

	err = checkConfig(&configYml)
	if err != nil {
		return nil, err
	}

	appConfig, err := marshalConfig(&configYml)
	if err != nil {
		return nil, err
	}

	err = appConfig.loadEzdConstants()
	if err != nil {
		return nil, err
	}

	appConfig.propagateTransitiveConfigs()

	return appConfig, nil
}

var errMarshalConfig = errors.New("MarshalConfig")

func ErrMarshalConfig(msg string) error {
	return fmt.Errorf("%w: %s", errMarshalConfig, msg)
}

// Initializes an `*AppConfiguration`.
//
// The given `config_yaml *string` is unmarshalled into `*AppConfiguration`.
func marshalConfig(configYml *string) (*AppConfiguration, error) {
	config := AppConfiguration{}

	err := yaml.UnmarshalWithOptions([]byte(*configYml), &config, yaml.DisallowUnknownField(), yaml.Strict())
	if err != nil {
		return nil, ErrMarshalConfig("yaml failed to unmarshal configYaml: " + err.Error())
	}

	return &config, nil
}

func (c *AppConfiguration) loadEzdConstants() error {
	for _, constant := range ezdconstants.GetAllEzdConstants() {
		env, err := constant.FindValue()
		if err != nil {
			return err
		}
		envString := string(constant) + "=" + env
		if !helpers.StringSliceContains(c.Env, envString) {
			c.Env = append(c.Env, envString)
		}
	}
	return nil
}

// Applies transitive config rules.
//
// This part seems to be impossible to realize through `viper` as
// defaults appearently cant be set through map keys.
//
// e.g.: `viper.SetDefault("lorem.ipsum[*].foo", "bar")`.
func (c *AppConfiguration) propagateTransitiveConfigs() {
	for name, seConfig := range c.Tasks.ShellExec {
		// Env
		for _, globalEnv := range c.Env {
			globalKey := strings.Split(globalEnv, "=")
			if seConfig.Env == nil {
				seConfig.Env = []string{}
			}
			match := false
			for _, localEnv := range seConfig.Env {
				localKey := strings.Split(localEnv, "=")
				if globalKey[0] == localKey[0] {
					match = true

					break
				}
			}
			if !match {
				seConfig.Env = append(seConfig.Env, globalEnv)
			}
		}
		c.Tasks.ShellExec[name] = seConfig
	}
	for name, dsConfig := range c.Tasks.DockerSeq {
		// Env
		for _, globalEnv := range c.Env {
			globalKey := strings.Split(globalEnv, "=")
			if dsConfig.Config.Env == nil {
				dsConfig.Config.Env = []string{}
			}
			match := false
			for _, localEnv := range dsConfig.Config.Env {
				localKey := strings.Split(localEnv, "=")
				if globalKey[0] == localKey[0] {
					match = true

					break
				}
			}
			if !match {
				dsConfig.Config.Env = append(dsConfig.Config.Env, globalEnv)
			}
		}
		// assign new values to the actual config object
		c.Tasks.DockerSeq[name] = dsConfig
	}
}

var errSetDefaultsForUninitialized = errors.New("SetDefaultsForUninitialized")

func ErrSetDefaultsForUninitialized(msg string) error {
	return fmt.Errorf("%w: %s", errSetDefaultsForUninitialized, msg)
}

// Merges all keys of the different command types and returns a list of them.
func (c *AppConfiguration) GetAllCommandNames() []string {
	buff := []string{}
	for cmdName := range c.Tasks.ShellExec {
		debug.Assert(!helpers.StringSliceContains(buff, cmdName), "Command is defined multiple times:", cmdName)
		buff = append(buff, cmdName)
	}
	for cmdName := range c.Tasks.DockerSeq {
		debug.Assert(!helpers.StringSliceContains(buff, cmdName), "Command is defined multiple times:", cmdName)
		buff = append(buff, cmdName)
	}

	return buff
}

func (c *AppConfiguration) TaskExists(name string) bool {
	return helpers.StringSliceContains(c.GetAllCommandNames(), name)
}

var errExpandRelativeVolumePaths = errors.New("ExpandRelativeVolumePaths")

func ErrExpandRelativeVolumePaths(msg string) error {
	return fmt.Errorf("%w: %s", errExpandRelativeVolumePaths, msg)
}

package config

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

var errFindConfigGetContent = errors.New("FindConfigGetContent")

func ErrFindConfigGetContent(msg string) error {
	return fmt.Errorf("%w: %s", errFindConfigGetContent, msg)
}

// Walks the current directory upwards and trys to find a `ezd.yml`.
//
// Will error if:
//
// - "/" is reached without finding any ezd.yml.
//
// - the maxdepth of 1024 is reached.
func findConfigGetContent() (configYml string, err error) {
	cwd, err := os.Getwd()
	if err != nil {
		return "", ErrFindConfigGetContent("os.Getwd() failed: " + err.Error())
	}
	depth := 0
	for cwd != "/" && depth < 1024 {
		content, err := ioutil.ReadFile(cwd + "/ezd.yml")
		if err != nil {
			cwd = filepath.Join(cwd, "..")
			depth++

			continue
		}
		err = os.Chdir(cwd)
		if err != nil {
			return "", ErrFindConfigGetContent("os.Chdir(cwd) failed: " + err.Error())
		}

		return string(content), nil
	}

	return "", ErrFindConfigGetContent("failed to find any \"ezd.yml\" in the current dir or above")
}

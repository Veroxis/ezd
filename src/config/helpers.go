package config

import (
	"errors"
	"fmt"
	"strings"

	"github.com/goccy/go-yaml"
	"gitlab.com/veroxis/ezd/src/ezdconstants"
	"gitlab.com/veroxis/ezd/src/helpers"
)

// All constants defined in `EzdConstant` are being loaded.
//
// - they are used to replace "__`EzdConstant`__" within `config_yml` through their respective value.
//
// - they are exported through `os.Setenv`.
func loadConstants(configYml *string) error {
	for _, ezd := range ezdconstants.GetAllEzdConstants() {
		value, err := ezd.FindValue()
		if err != nil {
			return err
		}
		*configYml = strings.ReplaceAll(*configYml, "__"+string(ezd)+"__", value)
	}

	return nil
}

var errCheckConfig = errors.New("CheckConfig")

func ErrCheckConfig(msg string) error {
	return fmt.Errorf("%w: %s", errCheckConfig, msg)
}

// Checks if the yaml configuration is valid.
func checkConfig(configYml *string) error {
	tasks := []string{}
	if path, err := yaml.PathString("$.tasks.shell_exec"); err == nil {
		var shellExecTasks map[string]ShellExec
		err = path.Read(strings.NewReader(*configYml), &shellExecTasks)
		if err == nil {
			for taskName := range shellExecTasks {
				if helpers.StringSliceContains(tasks, taskName) {
					return ErrCheckConfig("task has multiple declarations: " + taskName)
				}
				tasks = append(tasks, taskName)
			}
		}
	}
	if path, err := yaml.PathString("$.tasks.docker_seq"); err == nil {
		var dockerSeqTasks map[string]DockerSeq
		err = path.Read(strings.NewReader(*configYml), &dockerSeqTasks)
		if err == nil {
			for taskName := range dockerSeqTasks {
				if helpers.StringSliceContains(tasks, taskName) {
					return ErrCheckConfig("task has multiple declarations: " + taskName)
				}
				tasks = append(tasks, taskName)
			}
		}
	}

	return nil
}

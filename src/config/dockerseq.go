package config

type DockerSeq struct {
	Config      DockerSeqConfig `yaml:"config,omitempty"`
	Description string          `yaml:"description,omitempty"`
}

type DockerSeqConfig struct {
	PrintSequenceSteps *bool               `yaml:"print_sequence_steps,omitempty" json:"print_sequence_steps,omitempty"`
	Verbose            *bool               `yaml:"verbose,omitempty" json:"verbose,omitempty"`
	Image              string              `yaml:"image,omitempty" json:"image,omitempty"`
	DockerExecShell    []string            `yaml:"docker_exec_shell,omitempty" json:"docker_exec_shell,omitempty"`
	Workdir            string              `yaml:"workdir,omitempty" json:"workdir,omitempty"`
	User               string              `yaml:"user,omitempty" json:"user,omitempty"`
	LocalUuser         *bool               `yaml:"local_user,omitempty" json:"local_user,omitempty"`
	Tty                *bool               `yaml:"tty,omitempty" json:"tty,omitempty"`
	Interactive        *bool               `yaml:"interactive,omitempty" json:"interactive,omitempty"`
	Pull               string              `yaml:"pull,omitempty" json:"pull,omitempty"`
	Rm                 *bool               `yaml:"rm,omitempty" json:"rm,omitempty"`
	Init               *bool               `yaml:"init,omitempty" json:"init,omitempty"`
	Volumes            []string            `yaml:"volumes,omitempty" json:"volumes,omitempty"`
	Publish            []string            `yaml:"publish,omitempty" json:"publish,omitempty"`
	Env                []string            `yaml:"env,omitempty" json:"env,omitempty"`
	EnvFile            []string            `yaml:"env_file,omitempty" json:"env_file,omitempty"`
	EnvFoward          []string            `yaml:"env_forward,omitempty" json:"env_forward,omitempty"`
	Cpus               string              `yaml:"cpus,omitempty" json:"cpus,omitempty"`
	Memory             string              `yaml:"memory,omitempty" json:"memory,omitempty"`
	Entrypoint         string              `yaml:"entrypoint,omitempty" json:"entrypoint,omitempty"`
	Name               string              `yaml:"name,omitempty" json:"name,omitempty"`
	Privileged         *bool               `yaml:"privileged,omitempty" json:"privileged,omitempty"`
	Network            string              `yaml:"network,omitempty" json:"network,omitempty"`
	Mount              []string            `yaml:"mount,omitempty" json:"mount,omitempty"`
	ReadOnly           *bool               `yaml:"read_only,omitempty" json:"read_only,omitempty"`
	Platform           string              `yaml:"platform,omitempty" json:"platform,omitempty"`
	Runtime            string              `yaml:"runtime,omitempty" json:"runtime,omitempty"`
	SecurityOpt        []string            `yaml:"security_opt,omitempty" json:"security_opt,omitempty"`
	Ulimit             string              `yaml:"ulimit,omitempty" json:"ulimit,omitempty"`
	VolumesFrom        []string            `yaml:"volumes_from,omitempty" json:"volumes_from,omitempty"`
	BackgroundTask     []string            `yaml:"background_task,omitempty" json:"background_task,omitempty"`
	Sequence           []DockerSeqExecStep `yaml:"sequence,omitempty" json:"sequence,omitempty"`
}

type DockerSeqExecStep struct {
	Workdir     string   `yaml:"workdir,omitempty" json:"workdir,omitempty"`
	User        string   `yaml:"user,omitempty" json:"user,omitempty"`
	LocalUser   *bool    `yaml:"local_user,omitempty" json:"local_user,omitempty"`
	Tty         *bool    `yaml:"tty,omitempty" json:"tty,omitempty"`
	Privileged  *bool    `yaml:"privileged,omitempty" json:"privileged,omitempty"`
	Interactive *bool    `yaml:"interactive,omitempty" json:"interactive,omitempty"`
	Commands    []string `yaml:"commands,omitempty" json:"commands,omitempty"`
}

package debug

import (
	"encoding/json"
	"fmt"
	"os"
	"reflect"
)

// Pretty-Print function for generic Objects.
func Dump(elem interface{}) {
	b, err := json.MarshalIndent(elem, "", "  ")
	if err != nil {
		fmt.Fprintf(os.Stdout, "error: %s\n", err)
	}
	fmt.Fprintf(os.Stdout, "%s %s\n", reflect.TypeOf(elem), string(b))
}

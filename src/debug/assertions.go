package debug

import "gitlab.com/veroxis/ezd/src/runtime"

// Assert that cond is true.
//
// Exit the application if it isn't.
func Assert(cond bool, msg ...interface{}) {
	if !cond {
		runtime.ErrorExit("Assertion failed", msg)
	}
}

// `Unreachable` asserts that the path it is called in never occurs.
func Unreachable(elem ...interface{}) {
	runtime.ErrorExit("Unreachable", elem)
}

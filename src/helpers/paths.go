package helpers

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
)

func FilePathExists(path string) bool {
	_, err := os.Stat(path)

	return !errors.Is(err, os.ErrNotExist)
}

var errPathRelativeToAbsolute = errors.New("PathRelativeToAbsolute")

func ErrPathRelativeToAbsolute(msg string) error {
	return fmt.Errorf("%w: %s", errPathRelativeToAbsolute, msg)
}

func PathRelativeToAbsolute(basePath string) (string, error) {
	expandedPath, err := filepath.Abs(basePath)
	if err != nil {
		return "", ErrPathRelativeToAbsolute(err.Error())
	}
	if !FilePathExists(expandedPath) {
		return "", ErrPathRelativeToAbsolute("path does not exist: " + expandedPath)
	}
	return expandedPath, nil
}

package helpers

import (
	"bytes"
	"errors"
	"fmt"

	"github.com/alecthomas/chroma/v2/quick"
	"github.com/goccy/go-yaml"
)

var errHighlightYaml = errors.New("HighlightYaml")

func ErrHighlightYaml(msg string) error {
	return fmt.Errorf("%w: %s", errHighlightYaml, msg)
}

func HighlightYaml(yml string) (string, error) {
	var buff bytes.Buffer
	err := quick.Highlight(&buff, yml, "yml", "terminal256", "doom-one")
	if err != nil {
		return "", ErrHighlightYaml(err.Error())
	}
	highlightedYaml := buff.String()

	return highlightedYaml, nil
}

var errAsYaml = errors.New("AsYaml")

func ErrAsYaml(msg string) error {
	return fmt.Errorf("%w: %s", errAsYaml, msg)
}

func AsYaml(elem interface{}) (string, error) {
	data, err := yaml.Marshal(elem)
	if err != nil {
		return "", ErrAsYaml(err.Error())
	}
	yamlConfig := string(data)

	return yamlConfig, nil
}

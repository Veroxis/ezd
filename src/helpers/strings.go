package helpers

import (
	"strings"

	"github.com/texttheater/golang-levenshtein/levenshtein"
)

const (
	levenshteinDistance = 5
)

func StringSliceContains(slice []string, needle string) bool {
	for _, element := range slice {
		if element == needle {
			return true
		}
	}

	return false
}

func IsSimilar(haystack string, needle string) bool {
	haystack = strings.ToLower(haystack)
	needle = strings.ToLower(needle)
	if strings.HasPrefix(haystack, needle) {
		return true
	}
	if strings.HasSuffix(haystack, needle) {
		return true
	}
	if strings.Contains(haystack, needle) {
		return true
	}
	distance := levenshtein.DistanceForStrings([]rune(haystack), []rune(needle), levenshtein.DefaultOptions)
	return distance < levenshteinDistance
}

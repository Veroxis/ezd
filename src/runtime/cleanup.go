package runtime

import (
	"sync"
)

var (
	cleanupMutex     sync.Mutex
	cleanupFunctions []func() error
)

// Register a new cleanup function to execute when the application shuts down.
func RegisterCleanupFunction(f func() error) {
	cleanupMutex.Lock()
	cleanupFunctions = append(cleanupFunctions, f)
	cleanupMutex.Unlock()
}

// Execute all registered cleanup functions and clear the cleanup function queue.
func Cleanup() {
	cleanupMutex.Lock()
	for _, f := range cleanupFunctions {
		err := f()
		if err != nil {
			PrintRuntimeError(true, "Error in Cleanup:", err)
		}
	}
	cleanupFunctions = make([]func() error, 0)
	cleanupMutex.Unlock()
}

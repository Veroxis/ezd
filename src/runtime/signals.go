package runtime

import (
	"log"
	"os"
	"os/signal"
)

// Creates a Go-Routine which listens for OS Signals and handles them.
//
// If an `os.Interrupt` occurs, the program will be shut down gracefully.
func RegisterSignalHandler() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(
		sigs,
		os.Interrupt,
	)
	done := make(chan bool, 1)
	go func() {
		sig := <-sigs
		log.Printf(ShellCodeColorRed+"***** Received Signal: %+v *****"+ShellCodeReset+"\n", sig)
		Cleanup()
		done <- true
	}()
}

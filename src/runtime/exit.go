package runtime

import (
	"os"
)

// Print an error message and exit the application gracefully.
func ErrorExit(msg ...interface{}) {
	PrintRuntimeError(true, msg...)
	Cleanup()
	os.Exit(1)
}

package runtime

import (
	"fmt"
	"os"
	"runtime"
	"strings"
)

var (
	EnableTracing = false
)

func PrintRuntimeError(trace bool, msg ...interface{}) {
	if trace {
		printTrace()
	}
	fmt.Fprintf(os.Stderr, "[%sezd%s] %sERROR%s: %s%v%s\n",
		ShellCodeColorGreen, ShellCodeReset, ShellCodeColorRed, ShellCodeReset, ShellCodeColorPurple, msg, ShellCodeReset,
	)
}

func printTrace() {
	if !EnableTracing {
		return
	}
	cwd, cwdErr := os.Getwd()
	for idx := 99; idx >= 0; idx-- {
		programCounter, file, line, ok := runtime.Caller(idx)
		if !ok {
			continue
		}
		if cwdErr == nil {
			// make the absolute path relative
			file = strings.ReplaceAll(file, cwd, ".")
		}
		fmt.Fprintf(os.Stderr, "%2d: %s%s%s:%d%s: %s%v%s\n",
			idx, ShellCodeUnderlined, ShellCodeColorBlue,
			file, line, ShellCodeReset, ShellCodeColorYellow,
			runtime.FuncForPC(programCounter).Name(), ShellCodeReset,
		)
	}
}

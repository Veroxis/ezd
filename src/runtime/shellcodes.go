package runtime

// Shell Codes have to be declared again for this specific package due to circular imports
const (
	ShellCodeReset       string = "\033[0m"
	ShellCodeBold        string = "\033[1m"
	ShellCodeUnderlined  string = "\033[4m"
	ShellCodeColorRed    string = "\033[31m"
	ShellCodeColorGreen  string = "\033[32m"
	ShellCodeColorYellow string = "\033[33m"
	ShellCodeColorBlue   string = "\033[34m"
	ShellCodeColorPurple string = "\033[35m"
)

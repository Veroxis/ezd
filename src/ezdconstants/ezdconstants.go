package ezdconstants

import (
	"errors"
	"fmt"
	"os"
	"os/user"

	"gitlab.com/veroxis/ezd/src/debug"
)

type EzdConstant string

const (
	EzdHostProjectRoot EzdConstant = "EZD_HOST_PROJECT_ROOT"
	EzdHostUsername    EzdConstant = "EZD_HOST_USERNAME"
	EzdHostUID         EzdConstant = "EZD_HOST_UID"
	EzdHostGID         EzdConstant = "EZD_HOST_GID"
)

func GetAllEzdConstants() []EzdConstant {
	return []EzdConstant{EzdHostProjectRoot, EzdHostUsername, EzdHostUID, EzdHostGID}
}

var errEzdConstFindValue = errors.New("EzdConstFindValue")

func ErrEzdConstFindValue(msg string) error {
	return fmt.Errorf("%w: %s", errEzdConstFindValue, msg)
}

func (c *EzdConstant) FindValue() (string, error) {
	switch *c {
	case EzdHostProjectRoot:
		currentDir, err := os.Getwd()
		if err != nil {
			return "", ErrEzdConstFindValue("os.Getwd() failed: " + err.Error())
		}

		return currentDir, nil
	case EzdHostUsername:
		user, err := user.Current()
		if err != nil {
			return "", ErrEzdConstFindValue("user.Current() failed: " + err.Error())
		}

		return user.Username, nil
	case EzdHostUID:
		user, err := user.Current()
		if err != nil {
			return "", ErrEzdConstFindValue("user.Current() failed: " + err.Error())
		}

		return user.Uid, nil
	case EzdHostGID:
		user, err := user.Current()
		if err != nil {
			return "", ErrEzdConstFindValue("user.Current() failed: " + err.Error())
		}

		return user.Gid, nil
	default:
		debug.Unreachable(ErrEzdConstFindValue("`EzdConstants` have not been matched exhaustively"))

		return "", nil
	}
}

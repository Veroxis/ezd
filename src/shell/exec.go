package shell

import (
	"errors"
	"fmt"
	"os"
	"os/exec"

	"gitlab.com/veroxis/ezd/src/runtime"
)

var VerboseMode = false

var errAttachedExec = errors.New("AttachedExec")

func ErrAttachedExec(msg string) error {
	return fmt.Errorf("%w: %s", errAttachedExec, msg)
}

// Convenience function to execute `exec.Command` while attaching
// `os.Stdin`, `os.Stderr` and `os.Stdout`.
//
// The `*exec.Cmd` is __always__ returned, even if an `error` occurs.
func AttachedExec(cmd string, args ...string) (*exec.Cmd, error) {
	if VerboseMode { // printing the command
		tmp := ""
		for _, s := range args {
			tmp += s + " "
		}
		fmt.Fprintf(os.Stdout, string(CodeColorBlue)+"+ "+cmd+" "+tmp+string(CodeReset)+"\n")
	}
	command := exec.Command(cmd, args...)
	command.Stdin = os.Stdin
	command.Stderr = os.Stderr
	command.Stdout = os.Stdout
	if err := command.Run(); err != nil {
		runtime.PrintRuntimeError(false, "Command failed: [", err, "]")

		return command, ErrAttachedExec(err.Error())
	}

	return command, nil
}

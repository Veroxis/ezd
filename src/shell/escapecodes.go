package shell

type ShellEscapeCode string

const (
	CodeReset       ShellEscapeCode = "\033[0m"
	CodeBold        ShellEscapeCode = "\033[1m"
	CodeUnderlined  ShellEscapeCode = "\033[4m"
	CodeColorRed    ShellEscapeCode = "\033[31m"
	CodeColorGreen  ShellEscapeCode = "\033[32m"
	CodeColorYellow ShellEscapeCode = "\033[33m"
	CodeColorBlue   ShellEscapeCode = "\033[34m"
	CodeColorPurple ShellEscapeCode = "\033[35m"
)

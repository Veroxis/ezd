## [1.9.3](https://gitlab.com/Veroxis/ezd/compare/v1.9.2...v1.9.3) (2022-05-31)


### Bug Fixes

* **deps:** go mod tidy ([4e06bc4](https://gitlab.com/Veroxis/ezd/commit/4e06bc45720e55859e04dc77db897405c7f4737e))

## [1.9.2](https://gitlab.com/Veroxis/ezd/compare/v1.9.1...v1.9.2) (2022-05-31)


### Bug Fixes

* **deps:** update module github.com/alecthomas/chroma to v2 ([d28049a](https://gitlab.com/Veroxis/ezd/commit/d28049a7b1d3d466e529405e382193bc2ff73264))

## [1.9.1](https://gitlab.com/Veroxis/ezd/compare/v1.9.0...v1.9.1) (2022-05-31)


### Bug Fixes

* **ci:** use convco to parse next release version ([179a5ab](https://gitlab.com/Veroxis/ezd/commit/179a5ab463c2ee8e0215e0477e24fa1c154c2484))

# [1.9.0](https://gitlab.com/Veroxis/ezd/compare/v1.8.2...v1.9.0) (2022-05-24)


### Features

* use zig linker ([3427fe6](https://gitlab.com/Veroxis/ezd/commit/3427fe605af349f770030b258ba7e884873fae1d))

## [1.8.2](https://gitlab.com/Veroxis/ezd/compare/v1.8.1...v1.8.2) (2022-05-24)


### Bug Fixes

* **ci:** less recursion in release archive, second approach ([2b37e9a](https://gitlab.com/Veroxis/ezd/commit/2b37e9a6364e937d6b915c83498894d65458cb28))

## [1.8.1](https://gitlab.com/Veroxis/ezd/compare/v1.8.0...v1.8.1) (2022-05-24)


### Bug Fixes

* **ci:** less recursion in release archive ([72065ca](https://gitlab.com/Veroxis/ezd/commit/72065ca2f0c2a4efdd5f4bb8382f29d757e42036))

# [1.8.0](https://gitlab.com/Veroxis/ezd/compare/v1.7.0...v1.8.0) (2022-05-23)


### Features

* **version:** output verbose version info in json format ([12e0f52](https://gitlab.com/Veroxis/ezd/commit/12e0f5242a20ecd0107f052adfadccb95a20b2ae))

# [1.7.0](https://gitlab.com/Veroxis/ezd/compare/v1.6.0...v1.7.0) (2022-05-23)


### Features

* **version:** change format of version subcommand ([c4f23dc](https://gitlab.com/Veroxis/ezd/commit/c4f23dccc7315cdbd4452b7d04bc9e16c8872f5c))

# [1.6.0](https://gitlab.com/Veroxis/ezd/compare/v1.5.2...v1.6.0) (2022-03-22)


### Features

* removed `docker-seq` config merging ([8d8a764](https://gitlab.com/Veroxis/ezd/commit/8d8a76423b4e501f565392d7fd6e75648dc76190))

## [1.5.2](https://gitlab.com/Veroxis/ezd/compare/v1.5.1...v1.5.2) (2022-03-20)


### Bug Fixes

* fixed generating version information in CI ([a11f1a7](https://gitlab.com/Veroxis/ezd/commit/a11f1a7775825253aa591251db53008746965baf))

## [1.5.1](https://gitlab.com/Veroxis/ezd/compare/v1.5.0...v1.5.1) (2022-02-22)


### Bug Fixes

* updated `docker-seq` version ([d108e80](https://gitlab.com/Veroxis/ezd/commit/d108e80bf488ce048985b4febd2a1f138ac02419))

# [1.5.0](https://gitlab.com/Veroxis/ezd/compare/v1.4.0...v1.5.0) (2022-02-18)


### Bug Fixes

* added boundary check in `env` variable parsing ([545025e](https://gitlab.com/Veroxis/ezd/commit/545025e855a8b2964b26cf75ab8fdebf51bb3894))


### Features

* updated `docker-seq` version ([8d3abc3](https://gitlab.com/Veroxis/ezd/commit/8d3abc3a4b6e3b5251499855881308f65d0ceb02))

# [1.4.0](https://gitlab.com/Veroxis/ezd/compare/v1.3.0...v1.4.0) (2022-02-17)


### Features

* updated `docker-seq` from `v1.0.0` to `v1.1.0` ([57aef29](https://gitlab.com/Veroxis/ezd/commit/57aef2921cb1a27153854fc3f07a4e0db0f07d4e))

# [1.3.0](https://gitlab.com/Veroxis/ezd/compare/v1.2.0...v1.3.0) (2022-02-13)


### Features

* switched to use fixed release of docker-seq ([073010d](https://gitlab.com/Veroxis/ezd/commit/073010da8067957c652747cb5ee8be25ef5ebc5a))

# [1.2.0](https://gitlab.com/Veroxis/ezd/compare/v1.1.0...v1.2.0) (2022-02-13)


### Features

* displaying `buildtime` and `commithash` for the version ([352a2b4](https://gitlab.com/Veroxis/ezd/commit/352a2b482e7a88047b669eda233a61c4382f7cca))

# [1.1.0](https://gitlab.com/Veroxis/ezd/compare/v1.0.0...v1.1.0) (2022-02-13)


### Features

* using `commit timestamp` instead of `semver` for `ezd version` ([fea53a9](https://gitlab.com/Veroxis/ezd/commit/fea53a9196ef628e512c578903401133b07b497a))

# 1.0.0 (2022-02-13)


### Bug Fixes

* added docker_run descriptions in `ezd describe` ([5776f12](https://gitlab.com/Veroxis/ezd/commit/5776f126ad3d2f8c4395b37bcb4a4b33fb39e0e4))
* added file which was forgotten in last commit ([2196688](https://gitlab.com/Veroxis/ezd/commit/2196688640e3ec677555197ed086dbdd6a73c2d7))
* added handling for errors ([3938ba3](https://gitlab.com/Veroxis/ezd/commit/3938ba35233a6b050c99916f4f78ef2f2554b630))
* added more checks for duplicate task declarations ([f70cd8d](https://gitlab.com/Veroxis/ezd/commit/f70cd8db17ce60bda26aaa93be6b9ecc73006b1c))
* added propagation of global env variables into shell_exec context ([ed2c9ff](https://gitlab.com/Veroxis/ezd/commit/ed2c9ff353ded37e46042ef02d61a7eed4d9a5f4))
* corrected docker-seq schema url ([f52ed78](https://gitlab.com/Veroxis/ezd/commit/f52ed783a0d4c6c47e8b4cc039d69abc3fcc7d9e))
* fixed argument forwarding for shell_exec ([5ae85d6](https://gitlab.com/Veroxis/ezd/commit/5ae85d6c8ffb1363605c9d693c6be4d039976524))
* fixed buggy property types to reflect `docker-seq` ([65f8453](https://gitlab.com/Veroxis/ezd/commit/65f84534fa4d2a3e687fd2197591a140b79f40aa))
* fixed message print issue without to be compatible with lint `forbidigo` ([1d6d73d](https://gitlab.com/Veroxis/ezd/commit/1d6d73d5cebe776ddb1442577894580b2619eaf9))
* fixed the `ezd run lint` task ([503bd54](https://gitlab.com/Veroxis/ezd/commit/503bd5479292130adaf2277cf39887829413c3c1))
* fixed the generic type handling within `func checkConfig(configYml)` ([dea321b](https://gitlab.com/Veroxis/ezd/commit/dea321b9de4eccd8e0575e1675b4761f777c1ece))
* made basic lints succeed ([487c897](https://gitlab.com/Veroxis/ezd/commit/487c897f021c68cc81014edee5238f9063b729f4))
* made the `--backtrace` work for config issues by initilizing before config is read ([2216530](https://gitlab.com/Veroxis/ezd/commit/2216530a8f1d5ee1574b47ed9e13834f93b97fc3))
* made the `init` command not search for any existing project ([12196a9](https://gitlab.com/Veroxis/ezd/commit/12196a949869a2506f2b49bf0af6973180417b82))
* mistyped filename `dist.tar.gz` -> `ezd.tar.gz` ([be5467a](https://gitlab.com/Veroxis/ezd/commit/be5467af573c9c617b49840ccf8e185dd8129e2e))
* moved the `display-commands` flag into the task runner subcommand ([e4d7b2f](https://gitlab.com/Veroxis/ezd/commit/e4d7b2f504cc0347e7ac934bcc39ed3d283a1093))
* overwrite default docker entrypoint ([bc08767](https://gitlab.com/Veroxis/ezd/commit/bc08767dbe210d0822a87b7cb5ef111c16aaf144))
* registering the docker cleanup hook before starting the container ([fe4d854](https://gitlab.com/Veroxis/ezd/commit/fe4d8542156206c9b3f0f7cfb7652df5a0917607))
* removed `go vet` from the `test` task ([980ff1d](https://gitlab.com/Veroxis/ezd/commit/980ff1d8e5605800ae3f01f0f8433f62c3ec1859))
* removed an error with quotes being put around `$KB_FORWARDED_ARGS` ([3652e0a](https://gitlab.com/Veroxis/ezd/commit/3652e0af0afb0e6a1ea0c04c71aa7513a696faa9))
* removed documentation of non-existing field ([6130f74](https://gitlab.com/Veroxis/ezd/commit/6130f74fba8b91e1bcf2f372a614beb52b5e7e28))
* removed double-append of docker run arguments ([e66666c](https://gitlab.com/Veroxis/ezd/commit/e66666c7af78cb03b713ab9479233f5f1c001947))
* renamed mentions of the path `extern` -> `third_party` ([8641716](https://gitlab.com/Veroxis/ezd/commit/8641716c183ba2043e8b4baa5888ef4ba00881f9))
* set a matching `Short` description for the `list` command ([d6a7269](https://gitlab.com/Veroxis/ezd/commit/d6a726995153ec94e31ae75e7e10c92c42868001))
* using the correct image and env variables in docker_run ([dae69e7](https://gitlab.com/Veroxis/ezd/commit/dae69e7144a875d2142d05a2af9ef52dc8a33464))


### Features

* added `list` command which simply prints all available `tasks` ([dbc6c03](https://gitlab.com/Veroxis/ezd/commit/dbc6c036152162727c53f5803ff5eb65bc90614c))
* added expansion of relative paths in `volume: []` mappings ([dd3ae0f](https://gitlab.com/Veroxis/ezd/commit/dd3ae0f16eb58e66c9eda8e2021b883f9de88823))
* added helpmenu if `task` was not found ([079bb6f](https://gitlab.com/Veroxis/ezd/commit/079bb6f4c1772bfc141cac69e2ce74de36dd8d6f))
* added linting command in kaboom.yml ([d381c58](https://gitlab.com/Veroxis/ezd/commit/d381c58f6f565834b980ea838bfbfe0f3e8da18d))
* added loading `docker-seq` config files from `ezd.yml` ([74ba0ee](https://gitlab.com/Veroxis/ezd/commit/74ba0ee0a9dc2fbf639ef61491df24b91f006a85))
* added multiple features ([d54267d](https://gitlab.com/Veroxis/ezd/commit/d54267d6dc74c74ae753f35b18c0a96cd3842897))
* added new command `describe` ([b81ad8c](https://gitlab.com/Veroxis/ezd/commit/b81ad8cfb838ca46c59205a74256dcae3d641f86))
* added semantic-release ([1d3c802](https://gitlab.com/Veroxis/ezd/commit/1d3c8028e089d093b3648b418229ac8314ce5078))
* added version command ([e47908d](https://gitlab.com/Veroxis/ezd/commit/e47908dfcdaed42e6567eb2ba5aecebd7d37a6fa))
* changed build arguments for smaller binary ([506413e](https://gitlab.com/Veroxis/ezd/commit/506413e0b97919b1b47e740e1ed03b6ffc632819))
* changed kaboom clean command ([b318c4e](https://gitlab.com/Veroxis/ezd/commit/b318c4ee74a0bf68e61b6d415f8d85ace483dd81))
* changed kaboom lint configuration ([5b15865](https://gitlab.com/Veroxis/ezd/commit/5b15865550a1346a5e6ecf5d4a618276402e4467))
* changed logging format ([1537ad0](https://gitlab.com/Veroxis/ezd/commit/1537ad08a1ef2d530a4e0d5f90d5abf41473055b))
* enabled settings more config flags in shell_exec ([304d470](https://gitlab.com/Veroxis/ezd/commit/304d470a079f15d04e3f4a6dc9176a180824e01a))
* improved `describe` command ([a597e31](https://gitlab.com/Veroxis/ezd/commit/a597e31a6dbb456bf7065492405a5d6e03486426))
* improved the message/logging system ([5699c2f](https://gitlab.com/Veroxis/ezd/commit/5699c2fbb1aabef3658007599cdcc10b0803b745))
* initial working version ([a2ac3bb](https://gitlab.com/Veroxis/ezd/commit/a2ac3bb69bb648382fbc5f884ba8403d5159f218))
* made the backtrace available through `--backtrace=true` ([b2385d0](https://gitlab.com/Veroxis/ezd/commit/b2385d054007a618a533b4acda25b82f2e8e9828))
* made tracing optional in PrintError function ([8a5770e](https://gitlab.com/Veroxis/ezd/commit/8a5770e8547f19ffbc226b4bceb33aa4e7b756ba))
* moved repository `github.com/remissio` -> `gitlab.com/veroxis` ([cd56b20](https://gitlab.com/Veroxis/ezd/commit/cd56b20200ecd6c23310c7ce0202b7b8cd8d92e8))
* renamed commands -> tasks ([b3e14a1](https://gitlab.com/Veroxis/ezd/commit/b3e14a1363750c0cb5ada065b23dedffdf5190e4))
* replaced `viper` with `yaml` ([ad751e2](https://gitlab.com/Veroxis/ezd/commit/ad751e25a0a2a9f3e6de9ff5444665e90f0e0f9e))

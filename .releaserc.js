module.exports = {
    branches: ['*'],
    plugins: [
        "@semantic-release/commit-analyzer",
        "@semantic-release/release-notes-generator",
        "@semantic-release/changelog",
        ['@semantic-release/git', {
            assets: ['CHANGELOG.md'],
        }],
        ['@semantic-release/gitlab', {
            assets: [
                {
                    label: "all-${nextRelease.version}.tar.gz",
                    path: "ezd.tar.gz"
                },
                {
                    label: "ezd-${nextRelease.version}-x86_64-unknown-linux-musl",
                    path: "dist/x86_64-unknown-linux-musl/ezd"
                },
            ]
        }]
    ],
};

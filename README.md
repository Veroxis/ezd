# `ezd` - eZ Docker Task Runner

`ezd` is an easy to configure docker-based task runner system.

## Getting started

1. Create an `ezd.yml` file in your project root.
2. Customize the file. Examples:
   - [Go] [ezd](<https://gitlab.com/Veroxis/ezd/-/blob/master/ezd.yml>)
   - [Rust] [docker-seq](<https://gitlab.com/Veroxis/docker-seq/-/blob/master/ezd.yml>)
   - [Rust] [jeeves](<https://gitlab.com/Veroxis/jeeves/-/blob/master/ezd.yml>)
   - [Typescript/Angular] [my homepage](<https://gitlab.com/Veroxis/homepage/-/blob/master/ezd.yml>)
3. Run your Tasks using `ezd run $TASK_NAME`
   - To get a short view of your tasks use `ezd describe`
   - To inspect the final configuration use `ezd describe --details`
   - To inspect a specific task use `ezd describe $TASK_NAME`

## How it works

### Project configuration

At first `ezd` will search for it's corresponding configuration file by searching the current path and traversing upwards until it finds an `ezd.yml` file.

The _project root_ is the directory in which `ezd.yml` is saved. All tasks executed by `ezd` are relative to the _project root_.

### shell_exec

`shell_exec` tasks do execute the given commands within a local shell.

### docker_seq

`ezd` provides integration for [docker-seq](<https://gitlab.com/Veroxis/docker-seq>). It can embed its configuration file and it as tasks.

## Installation

### Linux

Download pre-built binaries [here](<https://gitlab.com/Veroxis/ezd/-/releases>).

### MacOS

Mac binaries are not provided since they can't be built in gitlab CI yet.

To use this application under Mac you have to compile it yourself.

### Prerequisites

- [Install Rust](<https://www.rust-lang.org/tools/install>)
- [Install Go](<https://go.dev/doc/install>)

### Building

```sh
# clone the repo
git clone https://gitlab.com/Veroxis/ezd.git && cd ezd

# fetch third_party code
git clone https://gitlab.com/veroxis/docker-seq.git third_party/docker-seq

# build third_party code
cd third_party/docker-seq
cargo build --release -p docker-seq-c-lib
cd ../..

# copy the built files into `third_party/lib/`
mkdir -p third_party/lib
cp third_party/docker-seq/target/release/dockerseq.h third_party/lib/dockerseq.h
cp third_party/docker-seq/target/release/libdockerseq.a third_party/lib/libdockerseq.a

# build ezd
go build --ldflags '-linkmode external -extldflags "-w -s"' -o ./bin/ezd ./src/
```

## License

`ezd` is released under the Apache 2.0 license. See [LICENSE.txt](LICENSE.txt)

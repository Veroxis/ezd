#!/bin/sh

set -eux

go clean -modcache
go clean -cache
rm -rf ./.docker ./third_party

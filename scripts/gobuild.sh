#!/bin/sh

set -eux

go build --ldflags '-linkmode external -I "$(which zigcc)" -extldflags "-static -lunwind -w -s"' -o ./bin/ezd ./src/

#!/bin/sh

set -eux

go test --ldflags '-linkmode external -I "$(which zigcc)" -extldflags "-static -lunwind -w -s"' -race ./src/...
